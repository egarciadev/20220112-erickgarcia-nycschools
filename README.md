# 20220112-ErickGarcia-NYCSchools

Create a browser based or native app to provide information on NYC High schools.
1. Display a list of NYC High Schools.
2. Selecting a school should show additional information about the school

Bonus
1. Search/Filtering support on the NYC High Schools list screen.

![preview.png](./preview.png)

Getting started:
Sign up for an app token and place the string inside of Constants.kt file on this
project in order to be able to see the schools when you compile and run the
application.
https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2

Makes use of the following architecture components and libraries:
AndroidX
Google Paging Library
Retrofit
ViewModel
Hilt
