package com.egarcia.assignment.schools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.egarcia.assignment.schools.service.model.LoadDataCallBack
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.service.model.SATSchoolResults
import com.egarcia.assignment.schools.service.repository.SchoolsSATDataSource
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SchoolDetailsViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    /** Class under test: **/
    private lateinit var viewModel: SchoolDetailsViewModel

    @MockK
    private lateinit var savedStateHandle: SavedStateHandle

    @MockK
    private lateinit var schoolsSATDataSource: SchoolsSATDataSource

    @MockK
    private lateinit var observer: Observer<NetworkSchool>

    @MockK
    private lateinit var satResultsObserver: Observer<List<SATSchoolResults>>

    private val testSchool = createTestSchool()
    private val satSchoolResults = listOf(createSatSchoolResults())

    @Before
    fun before() {
        MockKAnnotations.init(this)
        every { observer.onChanged(any()) } answers { nothing }
        every { satResultsObserver.onChanged(any()) } answers { nothing }
        every { schoolsSATDataSource.getSchoolSatScores(any(), any()) } answers { nothing }
        viewModel = SchoolDetailsViewModel(schoolsSATDataSource, savedStateHandle)
        viewModel.schoolLiveData.observeForever(observer)
        viewModel.satSchoolResultsLiveData.observeForever(satResultsObserver)
    }

    @Test
    fun `Given a valid school, when setSchool is called then the schoolLiveData is updated accordingly`() {
        // Given
        val expectedValue = testSchool
        val captureSlot = slot<NetworkSchool>()
        every { observer.onChanged(capture(captureSlot)) } answers { nothing }

        // When
        viewModel.setSchool(expectedValue)

        // Then
        verify { observer.onChanged(any()) }
        assertEquals(expectedValue, captureSlot.captured)

    }

    @Test
    fun `Given a valid school, when the view model is initialized then its data source is queried with the appropriate parameters`() {
        // Given
        val testSchool = testSchool
        val captureSlotDbn = slot<String>()
        val captureSlotCallBack = slot<LoadDataCallBack<SATSchoolResults>>()
        val captureSatResults = slot<List<SATSchoolResults>>()
        every {
            schoolsSATDataSource.getSchoolSatScores(
                capture(captureSlotDbn),
                capture(captureSlotCallBack)
            )
        } answers { captureSlotCallBack.captured.onResult(satSchoolResults) }
        every { satResultsObserver.onChanged(capture(captureSatResults)) } answers { nothing }

        // When
        viewModel.setSchool(testSchool)

        // Then
        verify { schoolsSATDataSource.getSchoolSatScores(any(), any()) }
        assertEquals(testSchool.dbn, captureSlotDbn.captured)
        assertNotNull(captureSlotCallBack)
        verify { satResultsObserver.onChanged(any()) }
        assertEquals(satSchoolResults, captureSatResults.captured)
    }

    //region Helper functions

    private fun createSatSchoolResults(): SATSchoolResults {
        return SATSchoolResults(
            "02M260",
            "Clinton School Writers & Artists, M.S. 260",
            "50",
            "411",
            "373",
            "369"
        )
    }

    private fun createTestSchool(): NetworkSchool {
        val dbn = "02M260"
        val name = "Clinton School Writers & Artists, M.S. 260"
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview =
            "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra =
            "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        return NetworkSchool(dbn, name, location, overview, phone, web, extra)
    }
    //endregion
}
