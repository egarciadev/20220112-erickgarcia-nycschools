package com.egarcia.assignment.schools.service.repository

import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.utils.caseInsensitiveSOQLSearch
import io.mockk.*
import io.mockk.impl.annotations.MockK
import retrofit2.Callback
import org.junit.Before
import org.junit.Test

class SchoolsRepositoryTest {

    /** Class under test **/
    private lateinit var schoolsRepository: SchoolsRepository

    @MockK
    private lateinit var schoolsApi: SchoolsApi

    @Before
    fun init() {
        MockKAnnotations.init(this)
        schoolsRepository = SchoolsRepository(schoolsApi)
    }

    @Test
    fun `Given an empty filter, a valid start, count, and callback parameters, when getSchools is called, then the schoolsApi is called with the appropriate parameters`() {
        // Given
        val filter = ""
        val start = 0
        val count = 0
        val callback = mockk<Callback<List<NetworkSchool>>>()

        every { schoolsApi.getSchools(start, count) } answers { mockk(relaxed = true) }

        // When
        schoolsRepository.getSchools(filter, start, count, callback)

        // Then
        verify { schoolsApi.getSchools(start, count) }
    }

    @Test
    fun `Given a valid non-empty filter and column, when getSchools is called, then the schoolsApi is called with the appropriate parameters`() {
        // Given
        val filter = "02M260"
        val column = "school_name"
        val query = "x like y"
        val start = 0
        val count = 0
        val callback = mockk<Callback<List<NetworkSchool>>>()

        mockkStatic("com.egarcia.assignment.utils.UtilsKt")
        every { caseInsensitiveSOQLSearch(column, filter) } returns query
        every { schoolsApi.getSchools(query, start, count) } answers { mockk(relaxed = true) }

        // When
        schoolsRepository.getSchools(filter, start, count, callback)

        // Then
        verify { caseInsensitiveSOQLSearch(column, filter) }
        verify { schoolsApi.getSchools(query, start, count) }
    }

    @Test
    fun `Given a valid non-empty filter and a successful schoolsApi response, when getSATResults is called, then the schoolsApi is called with the appropriate parameters`() {
        // Given
        val filter = "02M260"
        val column = "school_name"
        val query = "x like y"
        val start = 0
        val count = 0
        val callback = mockk<Callback<List<NetworkSchool>>>()

        mockkStatic("com.egarcia.assignment.utils.UtilsKt")
        every { caseInsensitiveSOQLSearch(column, filter) } returns query
        every { schoolsApi.getSchools(any(), any(), any()) } answers { mockk(relaxed = true) }

        // When
        schoolsRepository.getSchools(filter, start, count, callback)

        // Then
        verify { caseInsensitiveSOQLSearch(column, filter) }
        verify { schoolsApi.getSchools(query, start, count) }
    }
}