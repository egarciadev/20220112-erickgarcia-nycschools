package com.egarcia.assignment.schools.view.ui

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.egarcia.assignment.R
import com.egarcia.assignment.schools.service.model.NetworkSchool
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolDetailsActivityUTest {

    private val testSchool = createTestSchool()

    @Test
    fun `Given a resumed activity, when school name is accessed then it returns the expected value`() {
        // Given
        val listSchoolsScenario = launch(SchoolsListActivity::class.java)
        listSchoolsScenario.onActivity {
            val intent = Intent(it, SchoolDetailsActivity::class.java)
            intent.putExtra(SchoolDetailsActivity.EXTRA_ID_SCHOOL, testSchool)
            val activityDetailsScenario = launch<SchoolDetailsActivity>(intent)

            // When
            activityDetailsScenario.moveToState(Lifecycle.State.RESUMED)

            activityDetailsScenario.onActivity { activityDetails ->

                // Then
                activityDetails.viewModel.schoolLiveData.value?.let { school ->
                    assertEquals(testSchool.phone, school.phone)
                } ?: run { fail() }

                assertEquals("NYC High Schools", activityDetails.title.toString())
            }

        }

    }

    @Test
    fun `Given a resumed activity, when getTitle is accessed then it returns the expected value`() {
        // Given
        val activityScenario = launch(SchoolDetailsActivity::class.java)

        // When
        activityScenario.moveToState(Lifecycle.State.RESUMED)

        // Then
        activityScenario.onActivity { activity ->
            assertEquals("NYC High Schools", activity.title.toString())
        }
    }

    private fun createTestSchool(): NetworkSchool {
        val dbn = "02M260"
        val name = "Clinton School Writers & Artists, M.S. 260"
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview =
            "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra =
            "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        return NetworkSchool(dbn, name, location, overview, phone, web, extra)
    }
}