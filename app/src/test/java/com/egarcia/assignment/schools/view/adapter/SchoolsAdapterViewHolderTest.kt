package com.egarcia.assignment.schools.view.adapter

import android.app.Application
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.egarcia.assignment.databinding.SchoolsListItemBinding
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.view.ui.SchoolDetailsActivity
import com.egarcia.assignment.schools.viewmodel.SchoolsListViewModel
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.robolectric.Robolectric.buildActivity

@RunWith(AndroidJUnit4::class)
class SchoolsAdapterViewHolderTest {

    /** Class under test **/
    private lateinit var schoolsAdapter: SchoolsAdapter

    private val appContext = getApplicationContext<Application>()

    @MockK
    private lateinit var mockViewModel: SchoolsListViewModel

    @MockK
    private lateinit var mockViewGroup: ViewGroup

    @MockK
    private lateinit var mockBinding: SchoolsListItemBinding

    @MockK
    private lateinit var mockTextView: TextView

    @MockK
    private lateinit var mockCardView: CardView

    private val testSchool = createTestSchool()

    @Before
    fun before() {
        MockKAnnotations.init(this)
        val activityController = buildActivity(SchoolDetailsActivity::class.java).setup()
        every { mockViewGroup.context } answers { activityController.get().baseContext }
        every { mockBinding.root } answers { CardView(appContext) }
        schoolsAdapter = spyk(SchoolsAdapter(mockViewModel), recordPrivateCalls = true)
        every { schoolsAdapter["getItem"](anyInt()) } returns testSchool
    }

    @Test
    fun `Given a schools item binding and a school object, when bind is called, then the appropriate network school object is passed to the view holder bind method`() {
        // Given
        val parent = object : ViewGroup(appContext) {
            override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {}
        }
        val binding = SchoolsListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val bindingSpy = spyk(binding, recordPrivateCalls = true)
        every { bindingSpy.name } returns mockTextView
        every { bindingSpy getProperty "name" } returns mockTextView

        val viewHolder = schoolsAdapter.ViewHolder(bindingSpy)
        val nameSlot = slot<String>()
        every { mockTextView.text = capture(nameSlot) } answers { nothing }
        every { mockBinding.root } answers { mockCardView }
        every { mockBinding.cardView } answers { mockCardView }

        // When
        viewHolder.bind(testSchool)

        // Then
        verify { mockTextView.setText(testSchool.name) }
        verify { mockCardView.setOnClickListener(any()) }
    }


    private fun createTestSchool(): NetworkSchool {
        val dbn = "02M260"
        val name = "Clinton School Writers & Artists, M.S. 260"
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview =
            "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra =
            "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        return NetworkSchool(dbn, name, location, overview, phone, web, extra)
    }

}