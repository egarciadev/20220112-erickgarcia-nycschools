package com.egarcia.assignment.schools.service.model

import org.junit.Assert.assertEquals
import org.junit.Test

class NetworkSchoolTest {
    @Test
    fun `Given valid school attributes, when isValidResponse is called, then it returns true`() {
        // Given
        val dbn = "02M260"
        val name = "Clinton School Writers & Artists, M.S. 260"
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra = "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        val school = NetworkSchool(dbn, name, location, overview, phone, web, extra)
        val expectedResult = true
        // When
        val result = school.isValidResponse()
        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `Given an invalid dbn attribute, when isValidResponse is called, then it returns false`() {
        // Given
        val dbn = ""
        val name = "Clinton School Writers & Artists, M.S. 260"
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra = "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        val school = NetworkSchool(dbn, name, location, overview, phone, web, extra)
        val expectedResult = false
        // When
        val result = school.isValidResponse()
        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `Given an invalid name attribute, when isValidResponse is called, then it returns false`() {
        // Given
        val dbn = "02M260"
        val name = ""
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra = "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        val school = NetworkSchool(dbn, name, location, overview, phone, web, extra)
        val expectedResult = false
        // When
        val result = school.isValidResponse()
        // Then
        assertEquals(expectedResult, result)
    }

    @Test
    fun `Given an invalid dbn and name attribute, when isValidResponse is called, then it returns false`() {
        // Given
        val dbn = ""
        val name = ""
        val location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"
        val overview = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities."
        val phone = "212-524-4360"
        val web = "www.theclintonschool.net"
        val extra = "CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee"
        val school = NetworkSchool(dbn, name, location, overview, phone, web, extra)
        val expectedResult = false
        // When
        val result = school.isValidResponse()
        // Then
        assertEquals(expectedResult, result)
    }

}
