package com.egarcia.assignment.schools.utils

import com.egarcia.assignment.utils.caseInsensitiveSOQLSearch
import com.egarcia.assignment.utils.caseSensitiveSOQLMatch
import org.junit.Assert.assertEquals
import org.junit.Test

class UtilsTest {

    @Test
    fun `Given a valid column and keyword when caseInsensitiveSOQLSearch then the expected result is returned`() {
        // Given
        val column = "dbn"
        val keyword = "02M260"
        val expected = "lower(dbn) like lower('%02M260%')"

        // When
        val result = caseInsensitiveSOQLSearch(column, keyword)

        // Then
        assertEquals(expected, result)
    }

    @Test
    fun `Given a valid column and keyword when caseSensitiveSOQLMatch then the expected result is returned`() {
        // Given
        val column = "dbn"
        val keyword = "02M260"
        val expected = "dbn in ('02M260')"

        // When
        val result = caseSensitiveSOQLMatch(column, keyword)

        // Then
        assertEquals(expected, result)
    }

}