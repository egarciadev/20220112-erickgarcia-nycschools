package com.egarcia.assignment.schools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.service.repository.SchoolsDataSource
import com.egarcia.assignment.schools.service.repository.SchoolsDataSourceFactory
import com.egarcia.assignment.schools.service.repository.SchoolsRepository
import com.egarcia.assignment.utils.PAGE_SIZE
import com.egarcia.assignment.utils.ProgressStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * A collection of schools Listings
 */
@HiltViewModel
class SchoolsListViewModel @Inject constructor(
    private val schoolsRepository: SchoolsRepository,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private var listings: LiveData<PagedList<NetworkSchool>>
    private var loadingStatus: LiveData<ProgressStatus>
    private var viewEvent: MutableLiveData<SchoolListViewEvent> = MutableLiveData()
    private val queryKeywords = MutableLiveData("")
    private var factory: SchoolsDataSourceFactory = SchoolsDataSourceFactory(schoolsRepository = schoolsRepository)
    private val config = PagedList.Config.Builder()
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPageSize(PAGE_SIZE)
            .build()

    init {
        loadingStatus = Transformations.switchMap(factory.dataSource, ::loadNetworkState)
        listings = Transformations.switchMap(queryKeywords) {
            if (it.isEmpty()) {
                LivePagedListBuilder(factory, config).setInitialLoadKey(0).build()
            } else {
                LivePagedListBuilder(SchoolsDataSourceFactory(it, schoolsRepository), config).build()
            }
        }
    }

    fun refresh() {
        factory.dataSource.value?.invalidate()
    }

    fun search(keywords: String) {
        queryKeywords.value = keywords
    }

    fun paginatedListings(): LiveData<PagedList<NetworkSchool>> {
        return listings
    }

    fun loadingStatus(): LiveData<ProgressStatus> {
        return loadingStatus
    }

    fun viewEvents(): LiveData<SchoolListViewEvent> {
        return viewEvent
    }

    private fun loadNetworkState(dataSource: SchoolsDataSource): MutableLiveData<ProgressStatus> {
        return dataSource.progressStatus()
    }

    fun onListItemClick(selectedSchool: NetworkSchool) {
        viewEvent.value = SchoolListViewEvent.LaunchSchoolDetails(selectedSchool)
    }

}