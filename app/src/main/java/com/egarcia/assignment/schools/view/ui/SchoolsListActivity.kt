package com.egarcia.assignment.schools.view.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.egarcia.assignment.R
import com.egarcia.assignment.databinding.ActivitySchoolsListBinding
import com.egarcia.assignment.schools.view.adapter.SchoolsAdapter
import com.egarcia.assignment.schools.viewmodel.SchoolListViewEvent
import com.egarcia.assignment.schools.viewmodel.SchoolsListViewModel
import com.egarcia.assignment.utils.APP_TOKEN
import com.egarcia.assignment.utils.ProgressStatus
import com.egarcia.assignment.view.BaseActivity
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

/**
 * Displays a list of Listings
 */
@AndroidEntryPoint
class SchoolsListActivity : BaseActivity(),
    androidx.appcompat.widget.SearchView.OnQueryTextListener {

    //region Member variables
    private val viewModel: SchoolsListViewModel by viewModels()
    private lateinit var adapter: SchoolsAdapter
    private lateinit var binding: ActivitySchoolsListBinding
    private lateinit var snackbar: Snackbar
    private lateinit var progressBar: ProgressBar
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolsListBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)
        binding.toolbar.title = title

        setupRecyclerView(binding.listLayout.rentalList)
        progressBar = binding.listLayout.progressBar
        snackbar = Snackbar.make(
            binding.coordinator,
            R.string.error_message_generic, Snackbar.LENGTH_INDEFINITE
        )
        snackbar.setAction(R.string.retry) { viewModel.refresh() }
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as androidx.appcompat.widget.SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(this@SchoolsListActivity)
        }
        return true
    }

    //endregion

    //region Private methods
    private fun setupRecyclerView(recyclerView: RecyclerView) {
        adapter = SchoolsAdapter(viewModel)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        initObservers()
    }

    private fun initObservers() {
        val listingsLiveData = viewModel.paginatedListings()
        val loadingLiveData = viewModel.loadingStatus()
        val eventsLiveData = viewModel.viewEvents()

        listingsLiveData.observe(this, { pagedList ->
            progressBar.visibility = View.GONE
            adapter.submitList(pagedList)
        })

        loadingLiveData.observe(this, { loadingStatus ->

            when (loadingStatus) {
                is ProgressStatus.Loading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is ProgressStatus.Success -> {
                    snackbar.dismiss()
                    progressBar.visibility = View.GONE
                }
                is ProgressStatus.Error -> {
                    //TODO: Given more time, parse http response to check for unauthorized response
                    // and add error handling for this scenario instead of doing this hardcoded
                    // string comparison.
                    if (APP_TOKEN == "INSERT_API_APP_TOKEN_HERE") {
                        snackbar.setText(R.string.error_message_invalid_token)
                    }
                    snackbar.show()
                    progressBar.visibility = View.GONE
                }
            }
        })

        eventsLiveData.observe(this, { schoolListEvent ->
            when (schoolListEvent) {
                is SchoolListViewEvent.LaunchSchoolDetails -> {
                    val intent = Intent(this, SchoolDetailsActivity::class.java)
                    intent.putExtra(SchoolDetailsActivity.EXTRA_ID_SCHOOL, schoolListEvent.school)
                    startActivity(intent)
                }
            }
        })
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                viewModel.search(query)
            }
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        hideKeyboard()
        return true // Search already handled in onQueryTextChange
    }

    override fun onQueryTextChange(query: String?): Boolean {
        query?.let { viewModel.search(it) }
        return true
    }
    //endregion
}
