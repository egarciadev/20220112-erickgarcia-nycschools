package com.egarcia.assignment.schools.view.callback

import androidx.recyclerview.widget.DiffUtil
import com.egarcia.assignment.schools.service.model.NetworkSchool

class SchoolsDiffCallback : DiffUtil.ItemCallback<NetworkSchool>() {
    override fun areItemsTheSame(oldItem: NetworkSchool, newItem: NetworkSchool): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: NetworkSchool, newItem: NetworkSchool): Boolean {
        return oldItem.name == newItem.name
    }
}
