package com.egarcia.assignment.schools.service.model

import com.google.gson.annotations.SerializedName

data class SATSchoolResults(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val name: String,
    @SerializedName("num_of_sat_test_takers")
    val numTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val averageReadingScore: String,
    @SerializedName("sat_writing_avg_score")
    val averageWritingScore: String,
    @SerializedName("sat_math_avg_score")
    val averageMathScore: String
)