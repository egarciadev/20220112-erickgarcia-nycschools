package com.egarcia.assignment.schools.service.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.utils.ProgressStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InvalidObjectException

/**
 * Source of listings for the pagination library
 */
class SchoolsDataSource(
        private val currentQuery: String,
        private val schoolsRepository: SchoolsRepository
) : PageKeyedDataSource<Int, NetworkSchool>() {

    private val progressStatus: MutableLiveData<ProgressStatus> = MutableLiveData()
    private val invalidResponseError = ProgressStatus.Error(InvalidObjectException("Invalid response"))

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, NetworkSchool>) {
        // Not required
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, NetworkSchool>) {
        val initialPage = 0
        progressStatus.postValue(ProgressStatus.Loading)
        schoolsRepository
                .getSchools(currentQuery, initialPage, params.requestedLoadSize, object : Callback<List<NetworkSchool>> {
                    override fun onFailure(call: Call<List<NetworkSchool>>?, throwable: Throwable?) {
                        progressStatus.postValue(ProgressStatus.Error(throwable))
                    }

                    override fun onResponse(call: Call<List<NetworkSchool>>?, response: Response<List<NetworkSchool>>?) {
                        response?.body()?.let { schoolsResponse ->
                            if (schoolsResponse.isNotEmpty()) {
                                callback.onResult(schoolsResponse, initialPage, initialPage + schoolsResponse.size)
                                progressStatus.postValue(ProgressStatus.Success)
                            } else {
                                progressStatus.postValue(invalidResponseError)
                            }
                        } ?: progressStatus.postValue(invalidResponseError)
                    }
                })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, NetworkSchool>) {
        progressStatus.postValue(ProgressStatus.Loading)
        schoolsRepository
                .getSchools(currentQuery, params.key, params.requestedLoadSize, object : Callback<List<NetworkSchool>> {
                    override fun onFailure(call: Call<List<NetworkSchool>>?, throwable: Throwable?) {
                        progressStatus.postValue(ProgressStatus.Error(throwable))
                    }

                    override fun onResponse(call: Call<List<NetworkSchool>>?, response: Response<List<NetworkSchool>>?) {
                        response?.body()?.let { schoolsResponse ->
                            if (schoolsResponse.isNotEmpty()) {
                                callback.onResult(schoolsResponse, params.key + schoolsResponse.size)
                                progressStatus.postValue(ProgressStatus.Success)
                            } else {
                                progressStatus.postValue(invalidResponseError)
                            }
                        } ?: progressStatus.postValue(invalidResponseError)
                    }
                })
    }

    fun progressStatus(): MutableLiveData<ProgressStatus> {
        return progressStatus
    }

}