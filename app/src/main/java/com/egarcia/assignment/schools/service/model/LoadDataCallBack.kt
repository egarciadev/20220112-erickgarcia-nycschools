package com.egarcia.assignment.schools.service.model

import androidx.annotation.NonNull

abstract class LoadDataCallBack<Value> {
    abstract fun onResult(@NonNull data: List<Value>)
}
