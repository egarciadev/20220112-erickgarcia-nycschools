package com.egarcia.assignment.schools.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.egarcia.assignment.databinding.SchoolsListItemBinding
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.view.callback.SchoolsDiffCallback
import com.egarcia.assignment.schools.viewmodel.SchoolsListViewModel

/**
 * Used to display Listing objects
 */
class SchoolsAdapter(val viewModel: SchoolsListViewModel) :
    PagedListAdapter<NetworkSchool, SchoolsAdapter.ViewHolder>(SchoolsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            SchoolsListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { item -> holder.bind(item) }
    }

    inner class ViewHolder(private val binding: SchoolsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(networkSchool: NetworkSchool) {
            binding.name.text = networkSchool.name
            binding.cardView.setOnClickListener {
                viewModel.onListItemClick(networkSchool)
            }
        }
    }
}