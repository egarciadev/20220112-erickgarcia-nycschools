package com.egarcia.assignment.schools.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.annotation.VisibleForTesting
import com.egarcia.assignment.databinding.ActivitySchoolDetailsBinding
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.viewmodel.SchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySchoolDetailsBinding
    @VisibleForTesting
    val viewModel: SchoolDetailsViewModel by viewModels()

    companion object {
        const val EXTRA_ID_SCHOOL = "com.egarcia.assignment.school"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        intent.extras?.let {
            viewModel.setSchool(
                it.get(EXTRA_ID_SCHOOL) as NetworkSchool
            )
        }
        initObservers()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initObservers() {
        viewModel.schoolLiveData.observe(this, { school ->
            binding.name.text = school.name
            binding.overview.text = school.overview
            binding.location.text = school.location
            binding.website.text = school.webUrl
            binding.phone.text = school.phone
        })
        viewModel.satSchoolResultsLiveData.observe(this, { satSchoolResults ->
            binding.satReading.text = satSchoolResults[0].averageReadingScore
            binding.satWriting.text = satSchoolResults[0].averageWritingScore
            binding.satMath.text = satSchoolResults[0].averageMathScore
        })

    }
}