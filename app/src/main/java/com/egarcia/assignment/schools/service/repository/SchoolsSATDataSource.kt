package com.egarcia.assignment.schools.service.repository

import androidx.lifecycle.MutableLiveData
import com.egarcia.assignment.schools.service.model.LoadDataCallBack
import com.egarcia.assignment.schools.service.model.SATSchoolResults
import com.egarcia.assignment.utils.ProgressStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InvalidObjectException

class SchoolsSATDataSource(
    private val schoolsRepository: SchoolsRepository
) {

    private val progressStatus: MutableLiveData<ProgressStatus> = MutableLiveData()
    private val invalidResponseError =
        ProgressStatus.Error(InvalidObjectException("Invalid response"))

    fun getSchoolSatScores(schoolDBN: String, callback: LoadDataCallBack<SATSchoolResults>) {
        progressStatus.postValue(ProgressStatus.Loading)
        schoolsRepository
            .getSATResults(schoolDBN, 0, 5, object :
                Callback<List<SATSchoolResults>> {
                override fun onFailure(call: Call<List<SATSchoolResults>>?, throwable: Throwable?) {
                    progressStatus.postValue(ProgressStatus.Error(throwable))
                }

                override fun onResponse(
                    call: Call<List<SATSchoolResults>>?,
                    response: Response<List<SATSchoolResults>>?
                ) {
                    response?.body()?.let { result ->
                        if (result.isNotEmpty()) {
                            callback.onResult(result)
                            progressStatus.postValue(ProgressStatus.Success)
                        } else {
                            progressStatus.postValue(invalidResponseError)
                        }
                    } ?: progressStatus.postValue(invalidResponseError)
                }
            })
    }

    fun progressStatus(): MutableLiveData<ProgressStatus> {
        return progressStatus
    }
}