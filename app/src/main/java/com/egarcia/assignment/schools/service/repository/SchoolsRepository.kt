package com.egarcia.assignment.schools.service.repository

import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.service.model.SATSchoolResults
import com.egarcia.assignment.utils.caseInsensitiveSOQLSearch
import com.egarcia.assignment.utils.caseSensitiveSOQLMatch
import retrofit2.Callback
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Contains the implementation details for retrieving Rentals from the network.
 */
@Singleton
class SchoolsRepository @Inject constructor(private val schoolsApi: SchoolsApi) {

    /**
     * Used to retrieve a list of schools
     * @param filter serves as a way to filter the list of items using the given keyword. Case sensitive.
     * @param start the index position of the first item to be retrieved.
     * @param count how many items will be retrieved.
     */
    fun getSchools(filter: String = "", start: Int, count: Int, callback: Callback<List<NetworkSchool>>) {
        if (filter.isEmpty()) {
            schoolsApi.getSchools(start, count).enqueue(callback)
        } else {
            val query = caseInsensitiveSOQLSearch("school_name", filter)
            schoolsApi.getSchools(query, start, count).enqueue(callback)
        }
    }

    /**
     * Used to retrieve a list of SAT results
     * @param filter serves as a way to filter the list of items using the given keyword. Case sensitive.
     * @param start the index position of the first item to be retrieved.
     * @param count how many items will be retrieved.
     */
    fun getSATResults(filter: String = "", start: Int, count: Int, callback: Callback<List<SATSchoolResults>>) {
        val query = caseSensitiveSOQLMatch("dbn", filter)
        schoolsApi.getSATResults(query, start, count).enqueue(callback)
    }

}