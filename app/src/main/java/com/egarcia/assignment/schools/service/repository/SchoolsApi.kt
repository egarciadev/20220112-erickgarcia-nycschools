package com.egarcia.assignment.schools.service.repository

import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.service.model.SATSchoolResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *  The interface for the rental web service
 */
interface SchoolsApi {

    /**
     * Get the list of schools listings from the API
     * using pagination.
     */
    @GET("/resource/s3k6-pzi2.json")
    fun getSchools(
        @Query("\$offset") start: Int,
        @Query("\$limit") count: Int
    ): Call<List<NetworkSchool>>

    /**
     * Get the list of schools listings from the API
     * using pagination.
     * The results will be filtered using the given query.
     */
    @GET("/resource/s3k6-pzi2.json")
    fun getSchools(
        @Query("\$where") query: String,
        @Query("\$offset") start: Int,
        @Query("\$limit") count: Int
    ): Call<List<NetworkSchool>>

    /**
     * Get the list of SAT schools results from the API
     * where the results will be filtered using the given query.
     */
    @GET("/resource/f9bf-2cp4.json")
    fun getSATResults(
        @Query("\$where") query: String,
        @Query("\$offset") start: Int,
        @Query("\$limit") count: Int
    ): Call<List<SATSchoolResults>>
}