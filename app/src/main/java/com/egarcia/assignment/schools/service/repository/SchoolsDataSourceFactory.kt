package com.egarcia.assignment.schools.service.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.egarcia.assignment.schools.service.model.NetworkSchool

class SchoolsDataSourceFactory(private val query: String = "", private val schoolsRepository: SchoolsRepository)
    : DataSource.Factory<Int, NetworkSchool>() {

    val dataSource = MutableLiveData<SchoolsDataSource>()

    override fun create(): DataSource<Int, NetworkSchool> {
        val dataSource = SchoolsDataSource(query, schoolsRepository)
        this.dataSource.postValue(dataSource)
        return dataSource
    }
}
