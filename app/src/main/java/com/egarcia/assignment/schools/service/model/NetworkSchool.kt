package com.egarcia.assignment.schools.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * Class which provides a model for a rental
 * @constructor Sets all properties for this rental
 * @property dbn a unique String identifier for this rental.
 * ...
 */
@Parcelize
data class NetworkSchool(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val name: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("overview_paragraph")
    val overview: String,
    @SerializedName("phone_number")
    val phone: String,
    @SerializedName("website")
    val webUrl: String,
    @SerializedName("extracurricular_activities")
    val extraActivities: String
) : Parcelable

fun NetworkSchool.isValidResponse(): Boolean {
    return dbn.isNotBlank() && name.isNotBlank()
}