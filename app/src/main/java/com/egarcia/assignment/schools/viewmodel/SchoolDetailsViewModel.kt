package com.egarcia.assignment.schools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.egarcia.assignment.schools.service.model.LoadDataCallBack
import com.egarcia.assignment.schools.service.model.NetworkSchool
import com.egarcia.assignment.schools.service.model.SATSchoolResults
import com.egarcia.assignment.schools.service.repository.SchoolsSATDataSource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val dataSource: SchoolsSATDataSource,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private var _satSchoolResultsLiveData = MutableLiveData<List<SATSchoolResults>>()
    private var _schoolLiveData = MutableLiveData<NetworkSchool>()
    var satSchoolResultsLiveData: LiveData<List<SATSchoolResults>> = _satSchoolResultsLiveData
    var schoolLiveData: LiveData<NetworkSchool> = _schoolLiveData

    private fun loadData() {
        _schoolLiveData.value?.let { school ->
            dataSource.getSchoolSatScores(
                school.dbn,
                object : LoadDataCallBack<SATSchoolResults>() {
                    override fun onResult(data: List<SATSchoolResults>) {
                        _satSchoolResultsLiveData.postValue(data)
                    }
                })
        }
    }

    fun setSchool(newSchool: NetworkSchool) {
        _schoolLiveData.value = newSchool
        loadData()
    }

}