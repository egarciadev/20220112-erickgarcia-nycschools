package com.egarcia.assignment.schools.viewmodel

import com.egarcia.assignment.schools.service.model.NetworkSchool

sealed class SchoolListViewEvent {
    data class LaunchSchoolDetails(val school: NetworkSchool) : SchoolListViewEvent()
}