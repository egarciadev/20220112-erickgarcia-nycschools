package com.egarcia.assignment.schools.viewmodel

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()