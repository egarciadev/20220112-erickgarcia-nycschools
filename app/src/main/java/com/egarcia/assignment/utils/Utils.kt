package com.egarcia.assignment.utils

/**
 * Creates a SOQL type search query on the given column using keyboard according
 * to the documentation.
 * See [SOQL Docs](https://dev.socrata.com/docs/datatypes/text) for more information.
 */
fun caseInsensitiveSOQLSearch(column: String, keyword: String) : String {
    return "lower($column) like lower('%$keyword%')"
}

fun caseSensitiveSOQLMatch(column: String, keyword: String) : String {
    return "$column in ('$keyword')"
}