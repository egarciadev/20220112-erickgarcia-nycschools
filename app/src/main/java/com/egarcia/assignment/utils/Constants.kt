package com.egarcia.assignment.utils

const val BASE_URL: String = "https://data.cityofnewyork.us"
//TODO: Given more time, obfuscate the API token using the NDK since it is harder to decompile
// or even better would be to retrieve the token from an api along with a nonce so it cannot be
// reused if it gets intercepted by a Man in the Middle attack.
const val APP_TOKEN: String = "INSERT_API_APP_TOKEN_HERE"
const val PAGE_SIZE = 5

sealed class ProgressStatus {
    object Success : ProgressStatus()
    object Loading : ProgressStatus()
    data class Error(val throwable: Throwable?) : ProgressStatus()
}