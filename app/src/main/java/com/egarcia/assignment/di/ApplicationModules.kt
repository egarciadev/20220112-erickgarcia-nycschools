package com.egarcia.assignment.di

import com.egarcia.assignment.schools.service.repository.SchoolsApi
import com.egarcia.assignment.schools.service.repository.SchoolsRepository
import com.egarcia.assignment.schools.service.repository.SchoolsSATDataSource
import com.egarcia.assignment.utils.APP_TOKEN
import com.egarcia.assignment.utils.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.Request


@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun providesSchoolsSATDataSource(
        schoolsRepository: SchoolsRepository
    ): SchoolsSATDataSource {
        return SchoolsSATDataSource(schoolsRepository)
    }

    @Provides
    fun providesRentalRepository(
        schoolsApi: SchoolsApi
    ): SchoolsRepository {
        return SchoolsRepository(schoolsApi)
    }

    @Provides
    fun providesOKHTTPClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val request: Request = chain.request().newBuilder()
                .addHeader("X-App-Token", APP_TOKEN).build()
            chain.proceed(request)
        }
        return httpClient.build()
    }

    @Provides
    fun providesRentalApi(
        okHttpClient: OkHttpClient
    ): SchoolsApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(SchoolsApi::class.java)
    }
}