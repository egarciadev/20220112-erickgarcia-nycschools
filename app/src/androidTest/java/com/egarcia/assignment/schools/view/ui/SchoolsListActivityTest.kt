package com.egarcia.assignment.schools.view.ui


import android.view.View
import android.view.ViewGroup
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.egarcia.assignment.R
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class SchoolsListActivityTest {

    @Rule
    @JvmField
    var activityScenarioRule = ActivityScenarioRule(SchoolsListActivity::class.java)

    private lateinit var scenario: ActivityScenario<SchoolsListActivity>
    private lateinit var recyclerViewInteraction: ViewInteraction

    @Before
    fun init() {
        scenario = activityScenarioRule.scenario
        recyclerViewInteraction = onView(
            allOf(withId(R.id.rental_list), childAtPosition(withId(R.id.list_layout), 1))
        )
    }

    @Test
    fun recyclerViewIsDisplayed() {
        recyclerViewInteraction.check(matches(isDisplayed()))
    }

    @Test
    fun recyclerViewItemIsDisplayed() {
        val textView = onView(
            Matchers.allOf(
                withId(R.id.name), withText("Clinton School Writers & Artists, M.S. 260"),
                withParent(
                    Matchers.allOf(
                        withId(R.id.school_layout),
                        withParent(withId(R.id.card_view))
                    )
                ),
                isDisplayed()
            )
        )
        // With more time I would create an idling resource in order to avoid waiting a specific amount of time
        Thread.sleep(2000)
        textView.check(matches(withText("Clinton School Writers & Artists, M.S. 260")))
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
